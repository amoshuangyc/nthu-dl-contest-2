import torch
from torch import nn
from torch.nn import functional as F
from torchvision.models import resnet50

import util


class FPN(nn.Module):
    def __init__(self):
        super().__init__()
        backbone = resnet50(pretrained=False)
        self.pre = nn.Sequential(
            backbone.conv1,
            backbone.bn1,
            backbone.relu,
            backbone.maxpool,
        )
        self.layer1 = backbone.layer1
        self.layer2 = backbone.layer2
        self.layer3 = backbone.layer3
        self.layer4 = backbone.layer4
        self.layer5 = nn.Conv2d(2048, 256, [3, 3], padding=1, stride=2)
        self.layer6 = nn.Conv2d(256, 256, [3, 3], padding=1, stride=2)

        self.hconv5 = nn.Conv2d(2048, 256, [1, 1])
        self.hconv4 = nn.Conv2d(1024, 256, [1, 1])
        self.hconv3 = nn.Conv2d(512, 256, [1, 1])

        del backbone

    def forward(self, x):
        c1 = self.pre(x)
        c2 = self.layer1(c1)
        c3 = self.layer2(c2)
        c4 = self.layer3(c3)
        p5 = self.layer4(c4)
        p6 = self.layer5(p5)
        p7 = self.layer6(F.relu(p6))

        p5 = self.hconv5(p5)
        p4 = F.interpolate(p5, scale_factor=2) + self.hconv4(c4)
        p3 = F.interpolate(p4, scale_factor=2) + self.hconv3(c3)

        # print('c1:', c1.size())
        # print('c2:', c2.size())
        # print('c3:', c3.size())
        # print('c4:', c4.size())
        # print('p3:', p3.size())
        # print('p4:', p4.size())
        # print('p5:', p5.size())
        # print('p6:', p6.size())
        # print('p7:', p7.size())

        return p3, p4, p5, p6, p7


class RetinaNet(nn.Module):
    def __init__(self, n_class=20, n_anchor=9):
        super().__init__()
        self.fpn = FPN()
        self.loc_head = self.make_head(n_anchor * 4)
        self.cls_head = self.make_head(n_anchor * n_class)
        self.C = n_class
        self.A = n_anchor

    def forward(self, img_batch):
        loc_preds_batch = []
        cls_preds_batch = []

        fmps_batch = self.fpn(img_batch)  # (p3, p4, ..., p7)
        for fmp_batch in fmps_batch:
            N, _, fmpH, fmpW = fmp_batch.size()

            loc_pred_batch = self.loc_head(fmp_batch)  # [N, 4A, fmpH, fmpW]
            loc_pred_batch = loc_pred_batch.permute(0, 2, 3, 1)  # [N, fmpH, fmpW, 4A]
            loc_pred_batch = loc_pred_batch.contiguous()
            loc_pred_batch = loc_pred_batch.view(N, fmpH * fmpW * self.A, 4)

            cls_pred_batch = torch.sigmoid(self.cls_head(fmp_batch))  # [N, CA, fmpH, fmpW]
            cls_pred_batch = cls_pred_batch.permute(0, 2, 3, 1)  # [N, fmpH, fmpW, CA]
            cls_pred_batch = cls_pred_batch.contiguous()
            cls_pred_batch = cls_pred_batch.view(N, fmpH * fmpW * self.A, self.C)

            loc_preds_batch.append(loc_pred_batch)
            cls_preds_batch.append(cls_pred_batch)

        loc_preds_batch = torch.cat(loc_preds_batch, dim=1)
        cls_preds_batch = torch.cat(cls_preds_batch, dim=1)
        return loc_preds_batch, cls_preds_batch

    def make_head(self, out_c):
        return nn.Sequential(
            nn.Conv2d(256, 256, [3, 3], padding=1),
            nn.ReLU(),
            nn.Conv2d(256, 256, [3, 3], padding=1),
            nn.ReLU(),
            nn.Conv2d(256, 256, [3, 3], padding=1),
            nn.ReLU(),
            nn.Conv2d(256, 256, [3, 3], padding=1),
            nn.ReLU(),
            nn.Conv2d(256, out_c, [3, 3], padding=1),
        )


class FocalLoss(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, loc_true_b, cls_true_b, loc_pred_b, cls_pred_b,
                pos_mask_b, neg_mask_b):
        N = loc_true_b.size(0)
        loss = torch.zeros(N, device=loc_true_b.device)

        loc_true_b = loc_true_b[pos_mask_b]
        loc_pred_b = loc_pred_b[pos_mask_b]
        cls_true_b = cls_true_b[pos_mask_b | neg_mask_b]
        cls_pred_b = cls_pred_b[pos_mask_b | neg_mask_b]

        for i in range(N):
            # loc loss
            loc_true = loc_true_b[i].unsqueeze(0)
            loc_pred = loc_pred_b[i].unsqueeze(0)
            loc_loss = F.smooth_l1_loss(loc_pred, loc_true)

            # cls loss
            y = cls_true_b[i].float().view(1, -1)
            p = cls_pred_b[i].view(1, -1)
            cls_loss = F.binary_cross_entropy(p, y)
            g, eps = 2, 1e-8
            term0 = p * torch.log(y + eps) * ((1 - p)**g)
            term1 = (1 - p) * torch.log(1 - y + eps) * (p**g)
            cls_loss = -(term0 + term1).sum()

            # loss
            loss[i] = (loc_loss + cls_loss) / pos_mask_b[i].sum().float()

        return loss.mean()


if __name__ == '__main__':
    from dataset import VOC07
    from coder import Coder
    from torch.utils.data import DataLoader

    device = 'cpu'
    coder = Coder()
    ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
    img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
    voc07 = VOC07(coder, ann_path, img_dir, (320, 320))

    dl = DataLoader(voc07, batch_size=10, shuffle=True,
                    num_workers=3, drop_last=True)
    model = RetinaNet().to(device)
    loss_fn = FocalLoss().to(device)

    img_b, loc_true_b, cls_true_b, pos_mask_b, neg_mask_b = next(iter(dl))
    loss = loss_fn(loc_true_b, cls_true_b, loc_true_b, cls_true_b, pos_mask_b, neg_mask_b)
    print(loss)

    # for img_b, loc_true_b, cls_true_b, pos_mask_b, neg_mask_b in iter(dl):
    #     img_b = img_b.to(device)
    #     loc_true_b = loc_true_b.to(device)
    #     cls_true_b = cls_true_b.to(device)
    #     pos_mask_b = pos_mask_b.to(device)
    #     neg_mask_b = neg_mask_b.to(device)

    #     loc_pred_b, cls_pred_b = model(img_b)
    #     loss = loss_fn(loc_true_b, cls_true_b, loc_pred_b, cls_pred_b,
    #                    pos_mask_b, neg_mask_b)
    #     print(loss)
