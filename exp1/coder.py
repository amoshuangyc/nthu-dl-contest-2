import torch
import numpy as np
import util

class Coder:
    def __init__(self):
        self.img_size = (320, 320)
        self.n_class = 20
        self.fmps = [(40, 40), (20, 20), (10, 10), (5, 5), (3, 3)]
        self.areas = [32 * 32, 64 * 64, 128 * 128, 256 * 256, 512 * 512]
        self.ratios = [1 / 2, 1, 2 / 1]
        self.scales = [1, 2**(1 / 3), 2**(2 / 3)]
        self.anchors = self.get_anchors()

    def get_anchors(self):
        all_anchors = []

        for (fmpH, fmpW), area in zip(self.fmps, self.areas):
            # anchor sizes
            wh = []
            for ratio in self.ratios:
                w = np.sqrt(area / ratio)
                h = np.sqrt(area / 1)
                for scale in self.scales:
                    ww = h * scale
                    hh = w * scale
                    wh.append([ww, hh])
            A = len(wh)
            wh = torch.FloatTensor(wh)  # [#ratios * #scales, 2]
            wh = wh.view(1, 1, -1, 2)  # [1, 1, A, 2]
            wh = wh.expand(fmpH, fmpW, A, 2)

            # anchor centers
            strideH = self.img_size[0] / fmpH
            strideW = self.img_size[1] / fmpW
            xy = np.meshgrid(np.arange(fmpW), np.arange(fmpH))
            xy = np.stack(xy, axis=-1) + 0.5
            xy = xy * np.float32([strideW, strideH])
            xy = torch.from_numpy(xy).float()
            xy = xy.view(fmpH, fmpW, 1, 2)
            xy = xy.expand(fmpH, fmpW, A, 2)

            # anchor
            anchors = torch.cat([xy, wh], dim=3)  # [fmpH, fmpW, A, 4]
            anchors = anchors.view(-1, 4)  # [#anchor, 4]
            all_anchors.append(anchors)

        return torch.cat(all_anchors, dim=0)

    def encode(self, boxs, lbls):
        '''
        Args:
            boxs: (tensor) ground truth bounding boxs in ccwh format, sized [#obj, 4]
            lbls: (tensor) ground truth class label, sized [#obj,]
        Return:
            loc_true: (tensor) sized [#anchor, 4]
            cls_true: (tensor) sized [#anchor, #class]
            pos_mask: (tensor) mask of postive anchors, sized [#pos]
            neg_mask: (tensor) mask of negative anchors, sized [#neg]
        '''

        IOU = util.iou(self.anchors, boxs)

        # For each box, select the anchor with max overlap
        indices = torch.argmax(IOU, dim=0)
        IOU[IOU.argmax(dim=0), torch.arange(len(boxs))] = 1.0

        # For rest anchor, select the box with max overlap
        max_iou, indices = IOU.max(dim=1)
        boxs = boxs[indices]

        loc_true = torch.zeros_like(self.anchors)
        loc_true[:, 0] = (boxs[:, 0] - self.anchors[:, 0]) / self.anchors[:, 2]
        loc_true[:, 1] = (boxs[:, 1] - self.anchors[:, 1]) / self.anchors[:, 3]
        loc_true[:, 2] = torch.log(boxs[:, 2] / self.anchors[:, 2])
        loc_true[:, 3] = torch.log(boxs[:, 3] / self.anchors[:, 3])

        cls_true = util.to_one_hot(lbls[indices], self.n_class)
        pos_mask = (max_iou >= 0.5)
        neg_mask = (max_iou < 0.4)

        return loc_true, cls_true, pos_mask, neg_mask

    def decode(self, loc_pred, cls_pred, cls_thresh=0.5):
        '''
        Args:
            loc_pred: (tensor) regression prediction, sized in [#anchor, 4]
            cls_pred: (tensor) classification prediction, sized in [#anchor, #class]
        Return:
            boxs: (tensor) bounding boxes prediction in ccwh format, sized in [#anchor, 4]
            lbls: (tensor) classification prediction, sized in [#anchor, #class]
            cfds: (tensor) classification confidence, sized in [#anchor]
        '''

        boxs = torch.zeros_like(self.anchors)
        boxs[:, 0] = loc_pred[:, 0] * self.anchors[:, 2] + self.anchors[:, 0]
        boxs[:, 1] = loc_pred[:, 1] * self.anchors[:, 3] + self.anchors[:, 1]
        boxs[:, 2] = torch.exp(loc_pred[:, 2]) * self.anchors[:, 2]
        boxs[:, 3] = torch.exp(loc_pred[:, 3]) * self.anchors[:, 3]

        cfds, lbls = torch.max(torch.sigmoid(cls_pred), dim=1)
        return boxs, lbls, cfds


if __name__ == '__main__':
    import svg
    import dataset
    from torchvision.transforms import functional as tf

    coder = Coder()

    ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
    img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
    voc07 = dataset.VOC07(coder, ann_path, img_dir)

    img, loc_true, cls_true, pos_mask, neg_mask = voc07[0]

    boxs, lbls, cfds = coder.decode(loc_true, cls_true)
    boxs = util.ccwh2xyxy(boxs)

    boxs = boxs[pos_mask]
    lbls = lbls[pos_mask]
    cfds = cfds[pos_mask]

    keep = util.nms(boxs, cfds, 0.5)
    boxs = boxs[keep]
    lbls = lbls[keep]
    cfds = cfds[keep]

    print(boxs)

    img = tf.to_pil_image(img)
    w, h = img.size
    boxs = boxs.numpy()
    lbls = svg.tab20b[lbls.numpy() % 20]

    pos_anchors = coder.anchors[pos_mask]
    pos_anchors = util.ccwh2xyxy(pos_anchors)
    pos_anchors = pos_anchors.numpy()

    svg.save([svg.g([
        svg.pil(img),
        svg.bboxs(pos_anchors, lbls),
        svg.bboxs(boxs, 'orange'),
    ])], 'vis.svg', [h, w])
