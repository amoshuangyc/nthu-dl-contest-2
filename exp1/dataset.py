from pathlib import Path
import numpy as np
from PIL import Image
from tqdm import tqdm
import torch
from torch.utils.data import DataLoader
from torchvision.transforms import functional as tf

import util
from coder import Coder



class VOC07:
    def __init__(self, coder, ann_path, img_dir, img_size=(320, 320)):
        self.coder = coder
        self.ann_path = Path(ann_path).expanduser().resolve()
        self.img_dir = Path(img_dir).expanduser().resolve()
        self.img_size = img_size

        with self.ann_path.open() as f:
            data = [line.strip() for line in f.readlines()]

        self.anns = []
        for line in data:
            line = line.strip().split()
            vals = np.int32(line[1:]).reshape(-1, 5)
            self.anns.append({
                'img_name': line[0],
                'n_obj': len(vals),
                'boxs': vals[:, :-1],
                'lbls': vals[:, -1],
            })

    def __len__(self):
        return len(self.anns)

    def __getitem__(self, idx):
        ann = self.anns[idx]

        img_path = self.img_dir / ann['img_name']
        img = Image.open(img_path).convert('RGB')
        boxs = torch.FloatTensor(ann['boxs'])
        lbls = torch.LongTensor(ann['lbls'])

        srcW, srcH = img.size
        dstH, dstW = self.img_size
        img = img.resize((dstW, dstW))
        img = tf.to_tensor(img)

        boxs = torch.FloatTensor(ann['boxs'])
        boxs = boxs / torch.FloatTensor([srcW, srcH, srcW, srcH])
        boxs = boxs * torch.FloatTensor([dstW, dstH, dstW, dstH])
        # print(boxs)

        boxs = util.xyxy2ccwh(boxs)
        loc_true, cls_true, pos_mask, neg_mask = self.coder.encode(boxs, lbls)
        return img, loc_true, cls_true, pos_mask, neg_mask


if __name__ == '__main__':
    coder = Coder()
    ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
    img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
    voc07 = VOC07(coder, ann_path, img_dir)

    dl = DataLoader(voc07, batch_size=10, shuffle=True, num_workers=3, drop_last=True)
    for img_b, loc_true_b, cls_true_b, pos_mask_b, neg_mask_b in tqdm(iter(dl)):
        pass
