import json
from pathlib import Path
from random import randint
from datetime import datetime
from tqdm import tqdm

import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import Subset, ConcatDataset
from torch.utils.data import DataLoader
from torchvision.transforms import functional as tf

import svg
import util
from coder import Coder
from dataset import VOC07
from model import RetinaNet, FocalLoss


class Trainer:
    def __init__(self, log_dir, device, coder):
        self.log_dir = Path(log_dir).expanduser().resolve()
        self.device = device
        self.coder = coder
        self.model = RetinaNet().to(device)
        self.loss_fn = FocalLoss().to(device)
        self.optim = torch.optim.Adam(self.model.parameters(), lr=1e-3)

    def fit(self, train_set, valid_set, visul_set, max_epoch=30):
        self.train_loader = DataLoader(
            train_set, batch_size=32, shuffle=True, num_workers=4)
        self.valid_loader = DataLoader(
            valid_set, batch_size=32, shuffle=False, num_workers=3)
        self.visul_loader = DataLoader(
            visul_set, batch_size=32, shuffle=False, num_workers=3)

        n_train = len(train_set)
        n_valid = len(valid_set)
        n_visul = len(visul_set)

        for self.epoch in range(max_epoch):
            print(f'Epoch {self.epoch:03d}')
            self.epoch_dir = self.log_dir / f'{self.epoch:03d}'
            self.epoch_dir.mkdir(parents=True)

            with tqdm(total=n_train, desc='  Train', ascii=True) as pbar:
                train_metrics = self._train(pbar)
            with torch.no_grad():
                with tqdm(total=n_valid, desc='  Valid', ascii=True) as pbar:
                    valid_metrics = self._valid(pbar)
                # with tqdm(total=n_visul, desc='  Visul', ascii=True) as pbar:
                    # self.visul(pbar)

    def _train(self, pbar):
        self.model.train()
        metrics = {'loss': util.RunningAverage()}
        for img_b, loc_true_b, cls_true_b, pos_mask_b, neg_mask_b in \
            iter(self.train_loader):
            img_b = img_b.to(self.device)
            loc_true_b = loc_true_b.to(self.device)
            cls_true_b = cls_true_b.to(self.device)
            pos_mask_b = pos_mask_b.to(self.device)
            neg_mask_b = neg_mask_b.to(self.device)

            self.optim.zero_grad()
            loc_pred_b, cls_pred_b = self.model(img_b)
            loss = self.loss_fn(loc_true_b, cls_true_b, loc_pred_b, \
                            cls_pred_b, pos_mask_b, neg_mask_b)
            loss.backward()
            self.optim.step()

            metrics['loss'].update(loss.item())
            pbar.update(len(img_b))
            pbar.set_postfix(metrics)
        return metrics

    def _valid(self, pbar):
        self.model.eval()
        metrics = {'loss': util.RunningAverage()}
        for img_b, loc_true_b, cls_true_b, pos_mask_b, neg_mask_b in \
            iter(self.valid_loader):
            img_b = img_b.to(self.device)
            loc_true_b = loc_true_b.to(self.device)
            cls_true_b = cls_true_b.to(self.device)
            pos_mask_b = pos_mask_b.to(self.device)
            neg_mask_b = neg_mask_b.to(self.device)
            loc_pred_b, cls_pred_b = self.model(img_b)
            loss = self.loss_fn(loc_true_b, cls_true_b, loc_pred_b, \
                            cls_pred_b, pos_mask_b, neg_mask_b)
            metrics['loss'].update(loss.item())
            pbar.update(len(img_b))
            pbar.set_postfix(metrics)
        return metrics

    def visul(self, pbar):
        self.model.eval()
        for img_b, loc_true_b, cls_true_b, pos_mask_b, neg_mask_b in \
            iter(self.valid_loader):
            img_b_gpu = img_b.to(self.device)
            loc_pred_b_gpu, cls_pred_b_gpu = self.model(img_b_gpu)
            loc_pred_b = loc_pred_b_gpu.cpu()
            cls_pred_b = cls_pred_b_gpu.cpu()

            for i in range(len(img_b)):
                img = tf.to_pil_image(img_b[i])
                w, h = img.size

                true_boxs, true_lbls, _ = self.coder.decode(loc_true_b[i], cls_true_b[i])
                true_boxs = true_boxs.numpy()
                true_lbls = svg.tab20b[true_lbls.numpy()]

                pred_boxs, pred_lbls, _ = self.coder.decode(loc_pred_b[i], cls_pred_b[i])
                pred_boxs = pred_boxs.numpy()
                pred_lbls = svg.tab20b[pred_lbls.numpy()]

                vis0 = svg.g([
                    svg.pil(img),
                    svg.bboxs(true_boxs, true_lbls)
                ])
                vis1 = svg.g([
                    svg.pil(img),
                    svg.bboxs(true_boxs, true_lbls)
                ])
                path = self.epoch_dir / f'{pbar.n:03d}.svg'
                svg.save([vis0, vis1], str(path), [h, w], pad_val='black')
                pbar.update(1)


if __name__ == '__main__':
    coder = Coder()

    ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
    img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
    voc07 = VOC07(coder, ann_path, img_dir, (320, 320))

    pivot = len(voc07) * 9 // 10
    voc07train = Subset(voc07, list(range(len(voc07)))[:pivot])
    voc07valid = Subset(voc07, list(range(len(voc07)))[pivot:])
    train_vis = [randint(0, len(voc07train) - 1) for _ in range(50)]
    valid_vis = [randint(0, len(voc07valid) - 1) for _ in range(50)]
    voc07visul = ConcatDataset([
        Subset(voc07train, train_vis),
        Subset(voc07valid, valid_vis),
    ])


    log_dir = './log/{:%Y.%m.%d-%H:%M:%S}'.format(datetime.now())
    trainer = Trainer(log_dir, 'cuda', coder)
    trainer.fit(voc07train, voc07valid, voc07visul)
