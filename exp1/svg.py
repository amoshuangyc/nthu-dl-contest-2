from math import ceil
from io import BytesIO
from base64 import b64encode

import numpy as np
from PIL import Image
import svgwrite as sw


def g(elems):
    '''
    '''
    g = sw.container.Group()
    for elem in elems:
        g.add(elem)
    return g


def pil(img):
    '''
    '''
    w, h = img.size
    buf = BytesIO()
    img.save(buf, 'png')
    b64 = b64encode(buf.getvalue()).decode()
    href = 'data:image/png;base64,' + b64
    elem = sw.image.Image(href, (0, 0), width=w, height=h)
    return elem


def img(img):
    '''
    '''
    img = (img * 255).clip(0, 255).astype(np.uint8)
    img = Image.fromarray(img)
    return pil(img)


def save(elems, fname, size, per_row=-1, padding=2, pad_val=None):
    '''
    '''
    n_elem = len(elems)
    elems = [g.copy() for g in elems]
    imgH, imgW = size
    per_row = n_elem if per_row == -1 else per_row
    per_col = ceil(n_elem / per_row)
    gridW = per_row * imgW + (per_row - 1) * padding
    gridH = per_col * imgH + (per_col - 1) * padding

    svg = sw.Drawing(size=[gridW, gridH])
    if pad_val:
        svg.add(sw.shapes.Rect((0, 0), (gridW, gridH), fill=pad_val))
    for i in range(n_elem):
        c = (i % per_row) * (imgW + padding)
        r = (i // per_row) * (imgH + padding)
        elems[i].translate(c, r)
        svg.add(elems[i])

    with open(str(fname), 'w') as f:
        svg.write(f, pretty=True)


def to_png(src_path, dst_path, scale=2):
    '''
    '''
    import cairosvg
    pass

########################################

def bboxs(bboxes, colors='red', **extra):
    if type(bboxes) is np.ndarray:
        bboxes = bboxes.tolist()
    if type(colors) is np.ndarray:
        colors = colors.tolist()
    
    if isinstance(colors, str):
        colors = [colors]

    if 'stroke_width' not in extra:
        extra['stroke_width'] = 1
    if 'fill_opacity' not in extra:
        extra['fill_opacity'] = 0.0

    def transform(i):
        x1, y1, x2, y2 = bboxes[i]
        color = colors[i % len(colors)]
        args = {
            'insert': (round(x1), round(y1)),
            'size': (round(x2 - x1), round(y2 - y1)),
            'stroke': color,
            **extra
        }
        return sw.shapes.Rect(**args)

    return g([transform(i) for i in range(len(bboxes))])

##########################################

tab20 = np.array(['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'])

tab20b = np.array(['#393b79', '#5254a3', '#6b6ecf', '#9c9ede', '#637939', '#8ca252', '#b5cf6b', '#cedb9c', '#8c6d31', '#bd9e39', '#e7ba52', '#e7cb94', '#843c39', '#ad494a', '#d6616b', '#e7969c', '#7b4173', '#a55194', '#ce6dbd', '#de9ed6'])