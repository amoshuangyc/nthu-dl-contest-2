from pathlib import Path
import numpy as np
from PIL import Image
from tqdm import tqdm
import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torchvision.transforms import functional as tf

class VOC07:
    def __init__(self,
                 ann_path,
                 img_dir,
                 n_class=20,
                 img_size=(320, 320),
                 hmp_size=(80, 80)):
        self.ann_path = Path(ann_path).expanduser().resolve()
        self.img_dir = Path(img_dir).expanduser().resolve()
        self.n_class = n_class
        self.img_size = img_size
        self.hmp_size = hmp_size

        with self.ann_path.open() as f:
            data = [line.strip() for line in f.readlines()]

        self.anns = []
        for line in data:
            line = line.strip().split()
            vals = np.int32(line[1:]).reshape(-1, 5)
            self.anns.append({
                'img_name': line[0],
                'n_obj': len(vals),
                'boxs': vals[:, :-1],
                'lbls': vals[:, -1],
            })

    def __len__(self):
        return len(self.anns)

    def __getitem__(self, idx):
        ann = self.anns[idx]

        img_path = self.img_dir / ann['img_name']
        img = Image.open(img_path).convert('RGB')
        boxs = torch.FloatTensor(ann['boxs'])
        lbls = torch.LongTensor(ann['lbls'])

        srcW, srcH = img.size
        imgH, imgW = self.img_size
        img = img.resize((imgW, imgH))
        img = tf.to_tensor(img) # float, has been divided by 255

        hmpH, hmpW = self.hmp_size
        boxs = boxs / torch.FloatTensor([srcW, srcH, srcW, srcH])
        boxs = boxs * torch.FloatTensor([hmpW, hmpH, hmpW, hmpH])
        boxs = boxs.round().long()

        hmp = torch.zeros(20, *self.hmp_size)
        for tag in range(ann['n_obj']):
            (x1, y1, x2, y2), c = boxs[tag], lbls[tag]
            hmp[c, y1:y2, x1:x2] = 1.0

        return img, hmp


if __name__ == '__main__':
    ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
    img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
    voc07 = VOC07(ann_path, img_dir)

    import svg

    img, hmp = voc07[10]
    _, h, w = img.size()
    img = tf.to_pil_image(img)
    hmp = hmp.unsqueeze(1)
    hmp = F.interpolate(hmp, size=(h, w))
    hmp = [tf.to_pil_image(mp) for mp in hmp]

    svg.save([
        svg.pil(img),
        *[svg.pil(mp) for mp in hmp]
    ], 'vis.svg', (h, w), pad_val='white', per_row=7)