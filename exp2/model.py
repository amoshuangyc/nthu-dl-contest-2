import torch
from torch import nn
from torch.nn import functional as F
from torchvision.models import resnet50


class Model(nn.Module):
    def __init__(self, n_class=20):
        super().__init__()
        backbone = resnet50(pretrained=True)
        self.pre = nn.Sequential(
            backbone.conv1,
            backbone.bn1,
            backbone.relu,
            backbone.maxpool,
        )
        self.layer1 = backbone.layer1
        self.layer2 = backbone.layer2
        self.layer3 = backbone.layer3
        self.layer4 = backbone.layer4

        self.hconv5 = nn.Conv2d(2048, 256, [1, 1])
        self.hconv4 = nn.Conv2d(1024, 256, [1, 1])
        self.hconv3 = nn.Conv2d(512, 256, [1, 1])
        self.hconv2 = nn.Conv2d(256, 256, [1, 1])

        self.post = nn.Sequential(
            nn.Conv2d(256, 128, (3, 3), padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(128, n_class, (1, 1)),
            nn.BatchNorm2d(n_class),
            nn.Sigmoid()
        )

        del backbone

    def forward(self, x):
        c1 = self.pre(x)
        c2 = self.layer1(c1)
        c3 = self.layer2(c2)
        c4 = self.layer3(c3)
        c5 = self.layer4(c4)

        p5 = self.hconv5(c5)
        p4 = F.interpolate(p5, scale_factor=2) + self.hconv4(c4)
        p3 = F.interpolate(p4, scale_factor=2) + self.hconv3(c3)
        p2 = F.interpolate(p3, scale_factor=2) + self.hconv2(c2)

        return self.post(p2)


class Loss(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, hmp_pred, hmp_true):
        N = hmp_pred.size(0)
        # hmp_pred: [N, 20, 80, 80]
        # hmp_true: [N, 20, 80, 80]
        p = hmp_pred.view(N, -1)
        y = hmp_true.view(N, -1)
        term0 = (p * y).sum(dim=1)
        term1 = p.sum(dim=1) + y.sum(dim=1)
        smooth = 1.0
        loss = 1 - 2 * (term0 + smooth) / (term1 + smooth)
        return loss.mean()


if __name__ == '__main__':
    m = Model()
    x = torch.rand(10, 3, 320, 320)
    p = m(x)
    y = torch.rand(10, 20, 80, 80)
    print(Loss()(p, y))

    