import torch
import numpy as np
import util

class Coder:
    def __init__(self):
        self.img_size = (384, 384)
        self.n_class = 20
        self.fmps = [(24, 24)]
        self.areas = [16 * 16]
        self.ratios = [0.5, 1.0, 2.0]
        self.scales = [1, 4, 8, 12]
        self.A = len(self.ratios) * len(self.scales)
        self.anchors = self.get_anchors()

    def get_anchors(self):
        all_anchors = []

        for (fmpH, fmpW), area in zip(self.fmps, self.areas):
            # anchor sizes
            wh = []
            for ratio in self.ratios:
                w = np.sqrt(area / ratio)
                h = w * ratio
                for scale in self.scales:
                    ww = h * scale
                    hh = w * scale
                    wh.append([ww, hh])
            wh = torch.FloatTensor(wh)  # [#ratios * #scales, 2]
            wh = wh.view(1, 1, -1, 2)  # [1, 1, A, 2]
            wh = wh.expand(fmpH, fmpW, self.A, 2)

            # anchor centers
            strideH = self.img_size[0] / fmpH
            strideW = self.img_size[1] / fmpW
            xy = np.meshgrid(np.arange(fmpW), np.arange(fmpH))
            xy = np.stack(xy, axis=-1) + 0.5
            xy = xy * np.float32([strideW, strideH])
            xy = torch.from_numpy(xy).float()
            xy = xy.view(fmpH, fmpW, 1, 2)
            xy = xy.expand(fmpH, fmpW, self.A, 2)

            # anchor
            anchors = torch.cat([xy, wh], dim=3)  # [fmpH, fmpW, A, 4]
            anchors = anchors.view(-1, 4)  # [#anchor, 4]
            all_anchors.append(anchors)

        return torch.cat(all_anchors, dim=0)

    def encode(self, boxs):
        '''
        Args:
            boxs: (tensor) ground truth bounding boxs in ccwh format, sized [#obj, 4]
        Return:
            cls_true: (tensor) sized [#anchor, 1]
            loc_true: (tensor) sized [#anchor, 4]
        '''

        IOU = util.iou(self.anchors, boxs)

        # For each box, select the anchor with max overlap
        indices = torch.argmax(IOU, dim=0)
        IOU[IOU.argmax(dim=0), torch.arange(len(boxs))] = 1.0

        # For rest anchor, select the box with max overlap
        max_iou, indices = IOU.max(dim=1)
        boxs = boxs[indices]

        cls_true = torch.zeros(len(self.anchors), 1)
        pos_mask = (max_iou >= 0.6)
        cls_true[pos_mask, 0] = 1.0

        loc_true = torch.zeros_like(self.anchors)
        loc_true[:, 0] = (boxs[:, 0] - self.anchors[:, 0]) / self.anchors[:, 2]
        loc_true[:, 1] = (boxs[:, 1] - self.anchors[:, 1]) / self.anchors[:, 3]
        loc_true[:, 2] = torch.log(boxs[:, 2] / self.anchors[:, 2])
        loc_true[:, 3] = torch.log(boxs[:, 3] / self.anchors[:, 3])

        return cls_true, loc_true

    def decode(self, cls_pred, loc_pred, cls_thresh=0.5):
        '''
        Args:
            cls_pred: (tensor) classification prediction, sized in [#anchor, 1]
            loc_pred: (tensor) regression prediction, sized in [#anchor, 4]
        Return:
            boxs: (tensor) bounding boxes prediction in ccwh format, sized in [#anchor, 4]
            lbls: (tensor) classification prediction, sized in [#anchor, 1]
        '''
        boxs = torch.zeros_like(self.anchors)
        boxs[:, 0] = loc_pred[:, 0] * self.anchors[:, 2] + self.anchors[:, 0]
        boxs[:, 1] = loc_pred[:, 1] * self.anchors[:, 3] + self.anchors[:, 1]
        boxs[:, 2] = torch.exp(loc_pred[:, 2]) * self.anchors[:, 2]
        boxs[:, 3] = torch.exp(loc_pred[:, 3]) * self.anchors[:, 3]

        lbls = torch.round(cls_pred).long()
        return boxs, lbls


if __name__ == '__main__':
    pass
