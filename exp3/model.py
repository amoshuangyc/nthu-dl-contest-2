import torch
from torch import nn
from torch.nn import functional as F
from torchvision.models import resnet50


class Model(nn.Module):
    def __init__(self, n_class=20, A=9):
        super().__init__()

        backbone = resnet50(pretrained=False)
        self.pre = nn.Sequential(
            backbone.conv1,
            backbone.bn1,
            backbone.relu,
            backbone.maxpool,
        )
        self.layer1 = backbone.layer1
        self.layer2 = backbone.layer2
        self.layer3 = backbone.layer3
        self.layer4 = backbone.layer4

        self.hconv5 = nn.Conv2d(2048, 256, [1, 1])
        self.hconv4 = nn.Conv2d(1024, 256, [1, 1])
        self.hconv3 = nn.Conv2d(512, 256, [1, 1])

        self.cls_head = self.make_head(A * 1)
        self.loc_head = self.make_head(A * 4)

        del backbone

    def make_head(self, cout):
        return nn.Sequential(
            nn.Conv2d(256, 256, (1, 1), stride=2),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, (3, 3), padding=1), 
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, (3, 3), padding=1),
            nn.BatchNorm2d(256), 
            nn.ReLU(),
            nn.Conv2d(256, cout, (1, 1)),
            nn.BatchNorm2d(cout),
        )

    def forward(self, x):
        c1 = self.pre(x)
        c2 = self.layer1(c1)
        c3 = self.layer2(c2)
        c4 = self.layer3(c3)
        c5 = self.layer4(c4)

        p5 = self.hconv5(c5)
        p4 = F.interpolate(p5, scale_factor=2) + self.hconv4(c4)
        p3 = F.interpolate(p4, scale_factor=2) + self.hconv3(c3)

        cls_pred_b = torch.sigmoid(self.cls_head(p3))
        loc_pred_b = self.loc_head(p3)

        N = x.size(0)
        cls_pred_b = cls_pred_b.permute(0, 2, 3, 1)
        cls_pred_b = cls_pred_b.contiguous()
        cls_pred_b = cls_pred_b.view(N, -1, 1)
        loc_pred_b = loc_pred_b.permute(0, 2, 3, 1)
        loc_pred_b = loc_pred_b.contiguous()
        loc_pred_b = loc_pred_b.view(N, -1, 4)

        return cls_pred_b, loc_pred_b


class Loss(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, cls_pred_b, cls_true_b, loc_pred_b, loc_true_b):
        '''
        Args:
            cls_pred_b: (tensor) sized [N, fmpH * fmpW * A, 1]
            cls_true_b: (tensor) sized [N, fmpH * fmpW * A, 1]
            loc_pred_b: (tensor) sized [N, fmpH * fmpW * A, 4]
            loc_true_b: (tensor) sized [N, fmpH * fmpW * A, 4]
        '''
        N, device = cls_true_b.size(0), cls_true_b.device
        cls_loss = torch.zeros(N, device=device)
        loc_loss = torch.zeros(N, device=device)

        for i in range(N):
            cls_true, cls_pred = cls_true_b[i], cls_pred_b[i]
            loc_true, loc_pred = loc_true_b[i], loc_pred_b[i]

            pos_mask = cls_true[:, 0] >= 0.5
            neg_mask = cls_true[:, 0] < 0.5
            n_pos = min(128, pos_mask.sum())
            n_neg = min(512 - n_pos, neg_mask.sum())
            pos_mask = self.mask_sample(pos_mask, n_pos)
            neg_mask = self.mask_sample(neg_mask, n_neg)

            cls_pred = cls_pred[pos_mask | neg_mask] # [#anchor, 1]
            cls_true = cls_true[pos_mask | neg_mask] # [#anchor, 1]
            loc_pred = loc_pred[pos_mask] # [#anchor, 4]
            loc_true = loc_true[pos_mask] # [#anchor, 4]

            cls_loss[i] = F.binary_cross_entropy(cls_pred, cls_true)
            loc_loss[i] = F.smooth_l1_loss(loc_pred, loc_true)
            
        return cls_loss.mean() + 8 * loc_loss.mean()


    def mask_sample(self, mask, n_sample):
        '''
        Args:
            mask: (tensor) sized [#anchor]
            n_sample: (int)
        Return:
            mask: (tensor) sampled, sized [#anchor]
        '''
        idxs = torch.nonzero(mask)[:, 0]
        idxs = idxs[torch.randperm(len(idxs))]
        idxs = idxs[:n_sample]
        mask = torch.zeros_like(mask)
        mask[idxs] = 1
        return mask


if __name__ == '__main__':
    m = Model()
    x = torch.rand(10, 3, 320, 320)
    cls_pred, loc_pred = m(x)
    print(cls_pred.size())
    print(loc_pred.size())

