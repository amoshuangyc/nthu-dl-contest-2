import json
import random
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from datetime import datetime
import matplotlib as mpl
mpl.use('svg')
import matplotlib.pyplot as plt
plt.style.use('seaborn')

import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader, Subset, ConcatDataset
from torchvision.transforms import functional as tf

import svg
import util
from coder import Coder
from dataset import VOC07
from model import Model, Loss

coder = Coder()
ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
voc07 = VOC07(coder, ann_path, img_dir, img_size=coder.img_size)

n_sample = len(voc07)
pivot = n_sample * 9 // 10
voc07train = Subset(voc07, range(n_sample)[:pivot])
voc07valid = Subset(voc07, range(n_sample)[pivot:])
voc07visul = ConcatDataset([
    Subset(voc07train, random.sample(range(len(voc07train)), 50)),
    Subset(voc07valid, random.sample(range(len(voc07valid)), 50)),
])

train_loader = DataLoader(voc07train, batch_size=32, shuffle=True)
valid_loader = DataLoader(voc07valid, batch_size=32, shuffle=False)
visul_loader = DataLoader(voc07visul, batch_size=32, shuffle=False)

device = 'cuda'
model = Model(n_class=20, A=coder.A).to(device)
criterion = Loss().to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
log_dir = Path('./log/') / f'{datetime.now():%Y.%m.%d-%H:%M:%S}'


def train(pbar):
    model.train()
    metrics = {'loss': util.RunningAverage()}
    for img_b, cls_true_b, loc_true_b in iter(train_loader):
        img_b = img_b.to(device)
        cls_true_b = cls_true_b.to(device)
        loc_true_b = loc_true_b.to(device)

        optimizer.zero_grad()
        cls_pred_b, loc_pred_b = model(img_b)
        loss = criterion(cls_pred_b, cls_true_b, loc_pred_b, loc_true_b)
        loss.backward()
        optimizer.step()

        metrics['loss'].update(loss.item())
        pbar.set_postfix(metrics)
        pbar.update(img_b.size(0))
    return metrics


def valid(pbar):
    model.eval()
    metrics = {'loss': util.RunningAverage()}
    for img_b, cls_true_b, loc_true_b in iter(valid_loader):
        img_b = img_b.to(device)
        cls_true_b = cls_true_b.to(device)
        loc_true_b = loc_true_b.to(device)
        cls_pred_b, loc_pred_b = model(img_b)
        loss = criterion(cls_pred_b, cls_true_b, loc_pred_b, loc_true_b)
        metrics['loss'].update(loss.item())
        pbar.set_postfix(metrics)
        pbar.update(img_b.size(0))
    return {f'val_{k}': v for k, v in metrics.items()}


def visul(pbar, epoch):
    model.eval()
    epoch_dir = log_dir / f'{epoch:03d}'
    epoch_dir.mkdir(parents=True)

    for img_b, cls_true_b, loc_true_b in iter(visul_loader):
        cls_pred_b, loc_pred_b = model(img_b.to(device))
        cls_pred_b = cls_pred_b.cpu()
        loc_pred_b = loc_pred_b.cpu()

        for i in range(img_b.size(0)):
            _, _, h, w = img_b.size()
            cls_pred, cls_true = cls_pred_b[i], cls_true_b[i]
            loc_pred, loc_true = loc_pred_b[i], loc_true_b[i]
            img = svg.pil(tf.to_pil_image(img_b[i]))

            # true
            boxs, lbls = coder.decode(cls_true, loc_true)
            boxs = util.ccwh2xyxy(boxs)
            lbls = lbls.squeeze(1)
            mask = (lbls == 1)
            boxs = boxs[mask].numpy()
            vis0 = svg.g([
                img,
                svg.bboxs(boxs, 'orange'),
                svg.text(f'#positive: {mask.sum()}', (5, 10), 'white')
            ])

            # pred
            boxs, lbls = coder.decode(cls_pred, loc_pred)
            boxs = util.ccwh2xyxy(boxs)
            boxs[:, 0].clamp_(min=0, max=w)
            boxs[:, 1].clamp_(min=0, max=h)
            boxs[:, 2].clamp_(min=0, max=w)
            boxs[:, 3].clamp_(min=0, max=h)
            lbls = lbls.squeeze(1)
            mask = (lbls == 1)
            boxs = boxs[mask].numpy()
            vis1 = svg.g([
                img,
                svg.bboxs(boxs, 'green'),
                svg.text(f'#positive: {mask.sum()}', (5, 10), 'white')
            ])

            path = epoch_dir / f'{pbar.n:03d}.svg'
            svg.save([vis0, vis1], path, (h, w), pad_val='black')
            pbar.update(1)


def log(train_metrics, valid_metrics, epoch):
    json_path = log_dir / 'log.json'
    if json_path.exists():
        df = pd.read_json(json_path)
    else:
        df = pd.DataFrame()

    metrics = {'epoch': epoch, **train_metrics, **valid_metrics}
    df = df.append(metrics, ignore_index=True)
    df = df.astype('str').astype('float')
    with json_path.open('w') as f:
        json.dump(df.to_dict(orient='records'), f, indent=2)

    fig, ax = plt.subplots(dpi=100, figsize=(10, 6))
    df[['loss', 'val_loss']].plot(kind='line', ax=ax)
    fig.savefig(log_dir / 'loss.svg')
    plt.close()

    if df['val_loss'].idxmin() == epoch:
        torch.save(model, log_dir / 'model.pkl')


for epoch in range(50):
    print('Epoch', epoch)

    with tqdm(total=len(voc07train), desc='  Train', ascii=True) as pbar:
        train_metrics = train(pbar)

    with torch.no_grad():
        with tqdm(total=len(voc07valid), desc='  Valid', ascii=True) as pbar:
            valid_metrics = valid(pbar)
        with tqdm(total=len(voc07visul), desc='  Visul', ascii=True) as pbar:
            visul(pbar, epoch)
        log(train_metrics, valid_metrics, epoch)
