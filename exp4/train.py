import json
import random
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from datetime import datetime
import matplotlib as mpl
mpl.use('svg')
import matplotlib.pyplot as plt
plt.style.use('seaborn')

import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader, Subset, ConcatDataset
from torchvision.transforms import functional as tf

import svg
import util
from coder import Coder
from dataset import VOC07
from model import Model, Loss

coder = Coder()
ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
voc07 = VOC07(coder, ann_path, img_dir, img_size=coder.img_size)

n_sample = len(voc07)
pivot = n_sample * 9 // 10
voc07train = Subset(voc07, range(n_sample)[:pivot])
voc07valid = Subset(voc07, range(n_sample)[pivot:])
voc07visul = ConcatDataset([
    Subset(voc07train, random.sample(range(len(voc07train)), 50)),
    Subset(voc07valid, random.sample(range(len(voc07valid)), 50)),
])

train_loader = DataLoader(voc07train, batch_size=32, shuffle=True)
valid_loader = DataLoader(voc07valid, batch_size=32, shuffle=False)
visul_loader = DataLoader(voc07visul, batch_size=32, shuffle=False)

device = 'cuda'
model = Model(n_class=20, A=coder.A).to(device)
criterion = Loss().to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
log_dir = Path('./log/') / f'{datetime.now():%Y.%m.%d-%H:%M:%S}'


def train(pbar):
    model.train()
    metrics = {
        'loss': util.RunningAverage(),
        'cfd': util.RunningAverage(),
        'loc': util.RunningAverage(),
        'cls': util.RunningAverage(),
    }
    for img_b, cfd_true_b, loc_true_b, cls_true_b in iter(train_loader):
        img_b = img_b.to(device)
        cfd_true_b = cfd_true_b.to(device)
        loc_true_b = loc_true_b.to(device)
        cls_true_b = cls_true_b.to(device)

        optimizer.zero_grad()
        cfd_pred_b, loc_pred_b, cls_pred_b = model(img_b)
        loss, cfd_loss, loc_loss, cls_loss = criterion(
            cfd_pred_b, cfd_true_b, 
            loc_pred_b, loc_true_b, 
            cls_pred_b, cls_true_b)
        loss.backward()
        optimizer.step()

        metrics['loss'].update(loss.item())
        metrics['cfd'].update(cfd_loss.item())
        metrics['loc'].update(loc_loss.item())
        metrics['cls'].update(cls_loss.item())
        pbar.set_postfix(metrics)
        pbar.update(img_b.size(0))
    return metrics


def valid(pbar):
    model.eval()
    metrics = {
        'loss': util.RunningAverage(),
        'cfd': util.RunningAverage(),
        'loc': util.RunningAverage(),
        'cls': util.RunningAverage(),
    }
    for img_b, cfd_true_b, loc_true_b, cls_true_b in iter(valid_loader):
        img_b = img_b.to(device)
        cfd_true_b = cfd_true_b.to(device)
        loc_true_b = loc_true_b.to(device)
        cls_true_b = cls_true_b.to(device)
        cfd_pred_b, loc_pred_b, cls_pred_b = model(img_b)
        loss, cfd_loss, loc_loss, cls_loss = criterion(
            cfd_pred_b, cfd_true_b, 
            loc_pred_b, loc_true_b, 
            cls_pred_b, cls_true_b)
        metrics['loss'].update(loss.item())
        metrics['cfd'].update(cfd_loss.item())
        metrics['loc'].update(loc_loss.item())
        metrics['cls'].update(cls_loss.item())
        pbar.set_postfix(metrics)
        pbar.update(img_b.size(0))
    return {f'val_{k}': v for k, v in metrics.items()}


def get_vis(svg_pil, h, w, boxs, cfds, lbls):
    if boxs is None:
        return svg.g([
            svg_pil,
            svg.text('#positive: 0', (5, 10), 'white')
        ])
    
    boxs = util.ccwh2xyxy(boxs)
    boxs[:, 0].clamp_(min=0, max=w)
    boxs[:, 1].clamp_(min=0, max=h)
    boxs[:, 2].clamp_(min=0, max=w)
    boxs[:, 3].clamp_(min=0, max=h)
    boxs = boxs.numpy()
    lbls = lbls.numpy()
    n_pos = (cfds > 0.5).sum().item()
    colors = svg.cm.tab20a[lbls % 20]
    
    return svg.g([
        svg_pil,
        svg.bboxs(boxs, colors),
        svg.text(f'#positive: {n_pos}', (5, 10), 'white')
    ])


def visul(pbar, epoch):
    model.eval()
    epoch_dir = log_dir / f'{epoch:03d}'
    epoch_dir.mkdir(parents=True)

    for img_b, cfd_true_b, loc_true_b, cls_true_b in iter(visul_loader):
        cfd_pred_b, loc_pred_b, cls_pred_b = model(img_b.to(device))
        cfd_pred_b = cfd_pred_b.detach().cpu()
        loc_pred_b = loc_pred_b.detach().cpu()
        cls_pred_b = cls_pred_b.detach().cpu()

        for i in range(img_b.size(0)):
            _, _, h, w = img_b.size()
            cfd_pred, cfd_true = cfd_pred_b[i], cfd_true_b[i]
            loc_pred, loc_true = loc_pred_b[i], loc_true_b[i]
            cls_pred, cls_true = cls_pred_b[i], cls_true_b[i]
            img = svg.pil(tf.to_pil_image(img_b[i]))

            # true
            boxs, cfds, lbls = coder.decode(cfd_true, loc_true, cls_true, 0.5)
            vis0 = get_vis(img, h, w, boxs, cfds, lbls)
            # pred
            boxs, cfds, lbls = coder.decode(cfd_pred, loc_pred, cls_pred, 0.5)
            vis1 = get_vis(img, h, w, boxs, cfds, lbls)

            path = epoch_dir / f'{pbar.n:03d}.svg'
            svg.save([vis0, vis1], path, (h, w), pad_val='black')
            pbar.update(1)


def log(train_metrics, valid_metrics, epoch):
    json_path = log_dir / 'log.json'
    if json_path.exists():
        df = pd.read_json(json_path)
    else:
        df = pd.DataFrame()

    metrics = {'epoch': epoch, **train_metrics, **valid_metrics}
    df = df.append(metrics, ignore_index=True)
    df = df.astype('str').astype('float')
    with json_path.open('w') as f:
        json.dump(df.to_dict(orient='records'), f, indent=2)

    fig, ax = plt.subplots(1, 4, dpi=100, figsize=(40, 6))
    df[['loss', 'val_loss']].plot(kind='line', ax=ax[0])
    df[['cfd', 'val_cfd']].plot(kind='line', ax=ax[1])
    df[['loc', 'val_loc']].plot(kind='line', ax=ax[2])
    df[['cls', 'val_cls']].plot(kind='line', ax=ax[3])
    fig.savefig(log_dir / 'loss.svg')
    plt.close()

    if df['val_loss'].idxmin() == epoch:
        torch.save(model, log_dir / 'model.pkl')


for epoch in range(50):
    print('Epoch', epoch)

    with tqdm(total=len(voc07train), desc='  Train', ascii=True) as pbar:
        train_metrics = train(pbar)

    with torch.no_grad():
        with tqdm(total=len(voc07valid), desc='  Valid', ascii=True) as pbar:
            valid_metrics = valid(pbar)
        with tqdm(total=len(voc07visul), desc='  Visul', ascii=True) as pbar:
            visul(pbar, epoch)
        log(train_metrics, valid_metrics, epoch)
