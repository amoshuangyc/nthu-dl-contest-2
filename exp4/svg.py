from math import ceil
from io import BytesIO
from base64 import b64encode

import numpy as np
from PIL import Image
import svgwrite as sw


def g(elems):
    '''
    '''
    g = sw.container.Group()
    for elem in elems:
        if elem is not None:
            g.add(elem)
    return g


def pil(img):
    '''
    '''
    w, h = img.size
    buf = BytesIO()
    img.save(buf, 'png')
    b64 = b64encode(buf.getvalue()).decode()
    href = 'data:image/png;base64,' + b64
    elem = sw.image.Image(href, (0, 0), width=w, height=h)
    return elem


def img(img):
    '''
    '''
    img = (img * 255).clip(0, 255).astype(np.uint8)
    img = Image.fromarray(img)
    return pil(img)


def text(s, pos, color='orange', **extra):
    extra['fill'] = color
    return sw.text.Text(s, pos, **extra)


def save(elems, fname, size, per_row=-1, padding=2, pad_val=None):
    '''
    '''
    n_elem = len(elems)
    elems = [g.copy() for g in elems]
    imgH, imgW = size
    per_row = n_elem if per_row == -1 else per_row
    per_col = ceil(n_elem / per_row)
    gridW = per_row * imgW + (per_row - 1) * padding
    gridH = per_col * imgH + (per_col - 1) * padding

    svg = sw.Drawing(size=[gridW, gridH])
    if pad_val:
        svg.add(sw.shapes.Rect((0, 0), (gridW, gridH), fill=pad_val))
    for i in range(n_elem):
        c = (i % per_row) * (imgW + padding)
        r = (i // per_row) * (imgH + padding)
        elems[i].translate(c, r)
        svg.add(elems[i])

    with open(str(fname), 'w') as f:
        svg.write(f, pretty=True)


def to_png(src_path, dst_path, scale=2):
    '''
    '''
    import cairosvg
    pass


import matplotlib as mpl
import matplotlib.pyplot as plt


def get_color_map(plt_cm, n=10):
    colors = plt_cm(np.linspace(0, 1, n))
    colors = [mpl.colors.rgb2hex(c[:3]) for c in colors]
    return np.array(colors)


class cm:
    dark2 = get_color_map(plt.cm.Dark2, 10)
    tab20a = get_color_map(plt.cm.tab20, 20)
    tab20b = get_color_map(plt.cm.tab20b, 20)


def legend(colormap):
    pass


########################################


def bboxs(bboxes, colors='red', **extra):
    if type(bboxes) is np.ndarray:
        bboxes = bboxes.tolist()
    if type(colors) is np.ndarray:
        colors = colors.tolist()

    if isinstance(colors, str):
        colors = [colors]

    if 'stroke_width' not in extra:
        extra['stroke_width'] = 1
    if 'fill_opacity' not in extra:
        extra['fill_opacity'] = 0.0

    def transform(i):
        x1, y1, x2, y2 = bboxes[i]
        color = colors[i % len(colors)]
        args = {
            'insert': (round(x1), round(y1)),
            'size': (round(x2 - x1), round(y2 - y1)),
            'stroke': color,
            **extra
        }
        return sw.shapes.Rect(**args)

    return g([transform(i) for i in range(len(bboxes))])


def voc_bboxs(boxs, lbls, cfds, lw=1):
    boxs = boxs.tolist()
    lbls = lbls.tolist()
    cfds = cfds.tolist()
    names = [
        "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat",
        "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person",
        "pottedplant", "sheep", "sofa", "train", "tvmonitor"
    ]

    def transform(i):
        x1, y1, x2, y2 = map(round, boxs[i])
        color = cm.tab20a[lbls[i] % len(cm.tab20a)]
        text = f'{names[lbls[i]].capitalize()} {cfds[i]:.3f}'

        return g([
            sw.shapes.Rect(**{
                'insert': (x1, y1),
                'size': (x2 - x1, y2 - y1),
                'stroke': color,
                'stroke_width': lw,
                'fill_opacity': 0.0,
            }),
            sw.text.Text(text, **{
                'insert': (x1 + 5, y1 + 20),
                'font_size': 15,
                'font_weight': 'bold',
                'fill': color,
            })
        ])

    return g([transform(i) for i in range(len(boxs))])



##########################################