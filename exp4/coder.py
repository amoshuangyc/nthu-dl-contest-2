import torch
import numpy as np
import util

class Coder:
    def __init__(self):
        self.img_size = (384, 384)
        self.n_class = 20
        self.fmps = [(24, 24)]
        self.areas = [16 * 16]
        self.ratios = [0.5, 1.0, 2.0]
        self.scales = [1, 4, 8]
        self.A = len(self.ratios) * len(self.scales)
        self.anchors = self.get_anchors()

    def get_anchors(self):
        all_anchors = []

        for (fmpH, fmpW), area in zip(self.fmps, self.areas):
            # anchor sizes
            wh = []
            for ratio in self.ratios:
                w = np.sqrt(area / ratio)
                h = w * ratio
                for scale in self.scales:
                    ww = h * scale
                    hh = w * scale
                    wh.append([ww, hh])
            wh = torch.FloatTensor(wh)  # [#ratios * #scales, 2]
            wh = wh.view(1, 1, -1, 2)  # [1, 1, A, 2]
            wh = wh.expand(fmpH, fmpW, self.A, 2)

            # anchor centers
            strideH = self.img_size[0] / fmpH
            strideW = self.img_size[1] / fmpW
            xy = np.meshgrid(np.arange(fmpW), np.arange(fmpH))
            xy = np.stack(xy, axis=-1) + 0.5
            xy = xy * np.float32([strideW, strideH])
            xy = torch.from_numpy(xy).float()
            xy = xy.view(fmpH, fmpW, 1, 2)
            xy = xy.expand(fmpH, fmpW, self.A, 2)

            # anchor
            anchors = torch.cat([xy, wh], dim=3)  # [fmpH, fmpW, A, 4]
            anchors = anchors.view(-1, 4)  # [#anchor, 4]
            all_anchors.append(anchors)

        return torch.cat(all_anchors, dim=0)

    def encode(self, boxs, lbls):
        '''
        Args:
            boxs: (tensor) ground truth bounding boxs in ccwh format, sized [#obj, 4]
            lbls: (tensor) ground truth class labels, sized [#obj]
        Return:
            cfd_true: (tensor) sized [#anchor, 1]
            loc_true: (tensor) sized [#anchor, 4]
            cls_true: (tensor) sized [#anchor, #class]
        '''

        IOU = util.iou(self.anchors, boxs)

        # For each box, select the anchor with max overlap
        indices = torch.argmax(IOU, dim=0)
        IOU[IOU.argmax(dim=0), torch.arange(len(boxs))] = 1.0

        # For rest anchor, select the box with max overlap
        max_iou, indices = IOU.max(dim=1)
        boxs = boxs[indices]
        lbls = lbls[indices]

        cfd_true = torch.zeros(len(self.anchors), 1)
        pos_mask = (max_iou >= 0.5)
        cfd_true[pos_mask, 0] = 1.0

        loc_true = torch.zeros_like(self.anchors)
        loc_true[:, 0] = (boxs[:, 0] - self.anchors[:, 0]) / self.anchors[:, 2]
        loc_true[:, 1] = (boxs[:, 1] - self.anchors[:, 1]) / self.anchors[:, 3]
        loc_true[:, 2] = torch.log(boxs[:, 2] / self.anchors[:, 2])
        loc_true[:, 3] = torch.log(boxs[:, 3] / self.anchors[:, 3])

        cls_true = util.to_one_hot(lbls, self.n_class)

        return cfd_true, loc_true, cls_true

    def decode(self, cfd_pred, loc_pred, cls_pred, cfd_thresh=0.5):
        '''
        Args:
            cfd_pred: (tensor) positve confidence, sized [#anchor, 1]
            loc_pred: (tensor) anchor offset, sized [#anchor, 4]
            cls_pred: (tensor) anchor classification, sized [#anchor, #class]
        Return:
            boxs: (tensor) predicted bounding boxs in ccwh format, sized [#positive, 4]
            cfds: (tensor) predicted confidence, sized [#positive]
            lbls: (tensor) predicted class labels, sized [#positive]
        '''
        boxs = torch.zeros_like(self.anchors)
        boxs[:, 0] = loc_pred[:, 0] * self.anchors[:, 2] + self.anchors[:, 0]
        boxs[:, 1] = loc_pred[:, 1] * self.anchors[:, 3] + self.anchors[:, 1]
        boxs[:, 2] = torch.exp(loc_pred[:, 2]) * self.anchors[:, 2]
        boxs[:, 3] = torch.exp(loc_pred[:, 3]) * self.anchors[:, 3]

        pos_mask = (cfd_pred >= cfd_thresh).squeeze(dim=1)
        if pos_mask.sum() == 0:
            return None, None, None
        boxs = boxs[pos_mask]
        cfds = cfd_pred[pos_mask].squeeze(dim=1)
        lbls = torch.argmax(cls_pred[pos_mask], dim=1)

        return boxs, cfds, lbls


if __name__ == '__main__':
    pass
