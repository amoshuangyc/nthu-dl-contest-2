import numpy as np
from PIL import Image
from tqdm import tqdm
from pathlib import Path

import torch
from torch.utils.data import DataLoader
from torchvision.transforms import functional as tf

import util

class VOC07:
    def __init__(self, coder, ann_path, img_dir, img_size=(320, 320)):
        self.coder = coder
        self.ann_path = Path(ann_path).expanduser().resolve()
        self.img_dir = Path(img_dir).expanduser().resolve()
        self.img_size = img_size

        with self.ann_path.open() as f:
            data = [line.strip() for line in f.readlines()]

        self.anns = []
        for line in data:
            line = line.split()
            vals = np.int32(line[1:]).reshape(-1, 5)
            self.anns.append({
                'img_name': line[0],
                'n_obj': len(vals),
                'boxs': vals[:, :-1],
                'lbls': vals[:, -1],
            })

    def __len__(self):
        return len(self.anns)

    def __getitem__(self, idx):
        ann = self.anns[idx]

        img_path = self.img_dir / ann['img_name']
        img = Image.open(img_path).convert('RGB')
        boxs = torch.FloatTensor(ann['boxs'])
        lbls = torch.LongTensor(ann['lbls'])

        srcW, srcH = img.size
        dstH, dstW = self.img_size
        img = img.resize((dstW, dstW))
        img = tf.to_tensor(img)

        boxs = torch.FloatTensor(ann['boxs'])
        boxs = boxs / torch.FloatTensor([srcW, srcH, srcW, srcH])
        boxs = boxs * torch.FloatTensor([dstW, dstH, dstW, dstH])

        boxs = util.xyxy2ccwh(boxs)
        cfd_true, loc_true, cls_true = self.coder.encode(boxs, lbls)
        return img, cfd_true, loc_true, cls_true


if __name__ == '__main__':
    import svg
    from coder import Coder

    coder = Coder()
    ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
    img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
    voc07 = VOC07(coder, ann_path, img_dir, img_size=coder.img_size)

    img, cfd_true, loc_true, cls_true = voc07[200]
    print('cfd_true:', cfd_true.size())
    print('loc_true:', loc_true.size())
    print('cls_true:', cls_true.size())
    print('-' * 10)

    boxs, cfds, lbls = coder.decode(cfd_true, loc_true, cls_true)
    boxs = util.ccwh2xyxy(boxs)
    print('boxs:', boxs.size())
    print('cfds:', cfds.size())
    print('lbls:', lbls.size())
    print('-' * 10)

    # ---------------------------------

    img = tf.to_pil_image(img)
    w, h = img.size
    boxs = boxs.numpy()
    lbls = lbls.numpy()
    colors = svg.cm.tab20a[lbls % 20]

    mask = (cfd_true > 0.5).squeeze()
    pos_anchors = coder.anchors[mask]
    pos_anchors = util.ccwh2xyxy(pos_anchors)
    pos_anchors = pos_anchors.numpy()

    svg.save([svg.g([
        svg.pil(img),
        svg.bboxs(pos_anchors, colors, stroke_opacity=0.4),
        svg.bboxs(boxs, colors, stroke_width=3),
        svg.text(f'#box: {mask.sum()}', (5, 10), 'white')
    ])], 'vis.svg', [h, w])