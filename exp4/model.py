import torch
from torch import nn
from torch.nn import functional as F
from torchvision.models import resnet18


class Model(nn.Module):
    def __init__(self, n_class=20, A=9):
        super().__init__()
        self.n_class = n_class

        backbone = resnet18(pretrained=True)
        self.pre = nn.Sequential(
            backbone.conv1,
            backbone.bn1,
            backbone.relu,
            backbone.maxpool,
        )
        self.layer1 = backbone.layer1
        self.layer2 = backbone.layer2
        self.layer3 = backbone.layer3
        self.layer4 = backbone.layer4

        self.hconv5 = nn.Conv2d(512, 256, [1, 1])
        self.hconv4 = nn.Conv2d(256, 256, [1, 1])
        self.hconv3 = nn.Conv2d(128, 256, [1, 1])

        self.cfd_head = self.make_head(A * 1)
        self.loc_head = self.make_head(A * 4)
        self.cls_head = self.make_head(A * 128)

        del backbone

    def make_head(self, cout):
        return nn.Sequential(
            nn.Conv2d(256, 256, (1, 1), stride=2),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, (3, 3), padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, (3, 3), padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, cout, (1, 1)),
            nn.BatchNorm2d(cout),
        )

    def forward(self, x):
        c1 = self.pre(x)
        c2 = self.layer1(c1)
        c3 = self.layer2(c2)
        c4 = self.layer3(c3)
        c5 = self.layer4(c4)

        p5 = self.hconv5(c5)
        p4 = F.interpolate(p5, scale_factor=2) + self.hconv4(c4)
        p3 = F.interpolate(p4, scale_factor=2) + self.hconv3(c3)

        cfd_pred_b = torch.sigmoid(self.cfd_head(p3))
        loc_pred_b = self.loc_head(p3)
        cls_pred_b = self.cls_head(p3)

        N = x.size(0)
        cfd_pred_b = cfd_pred_b.permute(0, 2, 3, 1)
        cfd_pred_b = cfd_pred_b.contiguous()
        cfd_pred_b = cfd_pred_b.view(N, -1, 1) # [N, #anchor, 1]
        loc_pred_b = loc_pred_b.permute(0, 2, 3, 1)
        loc_pred_b = loc_pred_b.contiguous()
        loc_pred_b = loc_pred_b.view(N, -1, 4) # [N, #anchor, 4]
        cls_pred_b = cls_pred_b.permute(0, 2, 3, 1)
        cls_pred_b = cls_pred_b.contiguous()
        cls_pred_b = cls_pred_b.view(N, -1, 128) # [N, #anchor, 20]

        return cfd_pred_b, loc_pred_b, cls_pred_b


class Loss(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, cfd_pred_b, cfd_true_b, loc_pred_b, loc_true_b, cls_pred_b, cls_true_b):
        '''
        Args:
            cfd_pred_b: (tensor) sized [N, fmpH * fmpW * A, 1]
            cfd_true_b: (tensor) sized [N, fmpH * fmpW * A, 1]
            loc_pred_b: (tensor) sized [N, fmpH * fmpW * A, 4]
            loc_true_b: (tensor) sized [N, fmpH * fmpW * A, 4]
            cls_pred_b: (tensor) sized [N, fmpH * fmpW * A, 20]
            cls_true_b: (tensor) sized [N, fmpH * fmpW * A, 20]
        '''
        N, device = cls_true_b.size(0), cls_true_b.device
        cfd_loss = torch.zeros(N, device=device)
        loc_loss = torch.zeros(N, device=device)
        cls_loss = torch.zeros(N, device=device)

        for i in range(N):
            cfd_true, cfd_pred = cfd_true_b[i], cfd_pred_b[i]
            loc_true, loc_pred = loc_true_b[i], loc_pred_b[i]
            cls_true, cls_pred = cls_true_b[i], cls_pred_b[i]

            pos_mask = cfd_true[:, 0] >= 0.5
            neg_mask = cfd_true[:, 0] < 0.5
            n_pos = min(128, pos_mask.sum())
            n_neg = min(512 - n_pos, neg_mask.sum())
            pos_mask = self.mask_sample(pos_mask, n_pos)
            neg_mask = self.mask_sample(neg_mask, n_neg)

            cfd_pred = cfd_pred[pos_mask | neg_mask] # [#anchor, 1]
            cfd_true = cfd_true[pos_mask | neg_mask] # [#anchor, 1]
            loc_pred = loc_pred[pos_mask] # [#anchor, 4]
            loc_true = loc_true[pos_mask] # [#anchor, 4]
            cls_pred = cls_pred[pos_mask] # [#anchor, #class]
            cls_true = cls_true[pos_mask] # [#anchor, #class]

            cfd_loss[i] = F.binary_cross_entropy(cfd_pred, cfd_true)
            loc_loss[i] = F.smooth_l1_loss(loc_pred, loc_true)
            cls_loss[i] = F.cross_entropy(cls_pred, cls_true.argmax(dim=1))

        cfd_loss = cfd_loss.mean() * 1
        loc_loss = loc_loss.mean() * 8
        cls_loss = cls_loss.mean() * 0.2
        loss = cfd_loss + loc_loss + cls_loss
        return loss, cfd_loss, loc_loss, cls_loss

    def mask_sample(self, mask, n_sample):
        '''
        Args:
            mask: (tensor) sized [#anchor]
            n_sample: (int)
        Return:
            mask: (tensor) sampled, sized [#anchor]
        '''
        idxs = torch.nonzero(mask)[:, 0]
        idxs = idxs[torch.randperm(len(idxs))]
        idxs = idxs[:n_sample]
        mask = torch.zeros_like(mask)
        mask[idxs] = 1
        return mask


if __name__ == '__main__':

    from coder import Coder
    from dataset import VOC07
    from torch.utils.data import DataLoader

    coder = Coder()
    ann_path = '~/Downloads/all/pascal_voc_training_data.txt'
    img_dir = '~/Downloads/all/VOC2007train/JPEGImages/'
    voc07 = VOC07(coder, ann_path, img_dir, img_size=coder.img_size)
    loader = DataLoader(voc07, batch_size=2)

    model = Model(n_class=20, A=12)
    criterion = Loss()

    img_b, cfd_true_b, loc_true_b, cls_true_b = next(iter(loader))
    cfd_pred_b, loc_pred_b, cls_pred_b = model(img_b)

    loss, cfd_loss, loc_loss, cls_loss = criterion(
        cfd_pred_b, cfd_true_b,
        loc_pred_b, loc_true_b,
        cls_pred_b, cls_true_b)

    print(loss)
    print(cfd_loss)
    print(loc_loss)
    print(cls_loss)

