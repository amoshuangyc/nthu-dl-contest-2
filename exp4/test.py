import shutil
import numpy as np
from PIL import Image
from tqdm import tqdm
from pathlib import Path
from datetime import datetime

import torch
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torchvision.transforms import functional as tf

import svg
import util
from coder import Coder
from evaluate import evaluate


class VOC07Test:
    def __init__(self, img_dir, txt_path, img_size):
        self.img_dir = Path(img_dir).expanduser().resolve()
        self.img_size = img_size
        with Path(txt_path).expanduser().resolve().open() as f:
            names = [line.strip() for line in f.readlines()]
            self.img_paths = [self.img_dir / name for name in names]

    def __len__(self):
        # return len(self.img_paths)
        return 50

    def __getitem__(self, idx):
        path = self.img_paths[idx]
        img = Image.open(path).convert('RGB')
        srcW, srcH = img.size
        dstH, dstW = self.img_size
        img = img.resize((dstW, dstH))
        img = tf.to_tensor(img)
        meta = {'name': path.name, 'size': (srcW, srcH)}
        return img, meta

    @staticmethod
    def collate_fn(batch):
        img_b = torch.stack([item[0] for item in batch], dim=0)
        meta_b = list([item[1] for item in batch])
        return img_b, meta_b


coder = Coder()
img_dir = '~/Downloads/all/VOC2007test/JPEGImages/'
txt_path = '~/Downloads/all/pascal_voc_testing_data.txt'
img_size = (384, 384)
voc07test = VOC07Test(img_dir, txt_path, img_size)
test_loader = DataLoader(
    voc07test, batch_size=32, collate_fn=VOC07Test.collate_fn)

device = 'cuda'
model_path = './log/2018.11.18-14:36:27/model.pkl'
model = torch.load(model_path)
model = model.to(device)
log_dir = Path('./log/') / f'{datetime.now():%Y.%m.%d-%H:%M:%S}'
log_dir.mkdir()


def test(pbar, f):
    model.eval()
    for img_b, meta_b in iter(test_loader):
        cfd_pred_b, loc_pred_b, cls_pred_b = model(img_b.to(device))
        cfd_pred_b = cfd_pred_b.detach().cpu()
        loc_pred_b = loc_pred_b.detach().cpu()
        cls_pred_b = cls_pred_b.detach().cpu()

        for i in range(img_b.size(0)):
            meta = meta_b[i]
            dstH, dstW = meta['size']
            _, srcH, srcW = img_b[i].size()
            svg_path = (log_dir / meta['name']).with_suffix('.svg')

            cfd_pred = cfd_pred_b[i]  # [#anchor, 1]
            loc_pred = loc_pred_b[i]  # [#anchor, 4]
            cls_pred = cls_pred_b[i]  # [#anchor, 4]

            img = tf.to_pil_image(img_b[i])
            img = img.resize((dstW, dstH))
            img = svg.pil(img)
            
            boxs, cfds, lbls = coder.decode(cfd_pred, loc_pred, cls_pred, 0.5)
            if boxs is None:
                f.write(meta['name'])
                f.write('\n')
                svg.save([img, img], svg_path, (dstH, dstW))
                pbar.update(1)
                continue

            boxs = util.ccwh2xyxy(boxs)
            boxs = boxs / torch.FloatTensor([srcW, srcH, srcW, srcH])
            boxs = boxs * torch.FloatTensor([dstW, dstH, dstW, dstH])
            boxs = util.clamp(boxs, dstW, dstH)

            pred = []
            for cls_ in range(20):
                cls_mask = (lbls == cls_)
                if cls_mask.sum() == 0:
                    continue
                cls_boxs = boxs[cls_mask]
                cls_cfds = cfds[cls_mask]
                keep = util.nms(cls_boxs, cls_cfds)
                pred_boxs = cls_boxs[keep]
                pred_cfds = cls_cfds[keep].unsqueeze(1)
                pred_clss = torch.FloatTensor([cls_] * len(keep)).unsqueeze(1)
                pred.append(torch.cat((pred_boxs, pred_cfds, pred_clss), dim=1))
            pred = torch.cat(pred, dim=0)
            # print(pred)

            for (x1, y1, x2, y2, cfd, lbl) in pred.numpy().tolist():
                f.write('{} {:.3f} {:.3f} {:.3f} {:.3f} {} {:.3f}\n'.format(
                    meta['name'], x1, y1, x2, y2, int(lbl), cfd
                ))

            vis0 = svg.g([
                img,
                svg.voc_bboxs(boxs.numpy(), lbls.numpy(), cfds.numpy())
            ])
            vis1 = svg.g([
                img,
                svg.voc_bboxs(
                    pred[:, :4].numpy(),
                    pred[:, -1].long().numpy(),
                    pred[:, -2].numpy(),
                    lw=3
                )
            ])
            svg.save([vis0, vis1], svg_path, (dstH, dstW))

            pbar.update(1)


with tqdm(total=len(voc07test), desc='Pred', ascii=True) as pbar:
    with (log_dir / 'pred.txt').open('w') as f:
        test(pbar, f)

evaluate(str(log_dir / 'pred.txt'), str(log_dir / 'pred.csv'))